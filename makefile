server: source/server.c
	rm -f build/server
	gcc source/server.c -o build/server 

cmdClient: source/cmdClient.c
	rm -f build/cmdClient
	gcc source/cmdClient.c source/winCheck.c -o build/cmdClient

clean:
	rm -rf build/*
