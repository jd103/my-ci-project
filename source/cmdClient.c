#include <stdio.h>
#include <strings.h>

#include <sys/types.h>  //These headers are used for the server client libraries
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include "winCheck.h"

//Global board variable.
char board[3][3]  = {
	{'1','2','3'},
	{'4','5','6'},
	{'7','8','9'}
};

int socketNo;
char clientNo[15];

char input[50] = "127.0.0.1";
char clientPiece = 'O';
char serverPiece = 'X';
char headTail = 'T';

int first, socketNo;
int count = 0;
int winner = 0;

int clientScore = 0;
int serverScore = 0;

//__RESET_GAME__
//This function resets the global variables when the user presses play again.

void gameReset(){
	clientPiece = 'X';
	serverPiece = 'O';
	headTail = 'T';
	winner = 0;
	count = 0;
//For loop is used to iterate through the board replacing each character from 1 to 9.
	for(int i = 0; i < 3; i++) {
		for(int j = 0; j < 3; j++) {
			char x = ((i*3)+(j+1))+'0';
			board[i][j] = x;
		}
	}
}

//__END_RESET_GAME__

int serverInit(char ip[50]){
//These network structures are required to connect to the server.
//Sockaddr defines the generic data type for addresses.
    printf("Attemting to connect to server: %s\n", ip);

	struct sockaddr_in serv_addr;
	struct hostent *server;

//TCP (SOCK_STREAM) Socket is created.
	socketNo = socket(AF_INET, SOCK_STREAM, 0); // Make a new communication endpoint.
//This function obtains information about a domain name and stores it in a hostent struct
	server = gethostbyname(ip);

	int portno = 5000;

	serv_addr.sin_family = AF_INET; //Address Family INET is used for TCP.
//Copy byte sequence from server.h_addr_lst to serv_addr.sinaddr.s_addr.
	bcopy ((char*)(*server).h_addr_list [0], (char*)&serv_addr.sin_addr.s_addr, (*server).h_length);
//Change integer to network byte format for the structure.
	serv_addr.sin_port = htons(portno);

//Attempts to connect to the server, returns -1 if unsuccessful.
	if (connect(socketNo, (struct  sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
		printf("ERROR: Could not connect to the server\n");
		return -1;
	}

//If successful the function returns 0 and prints to the command line the client number.
	recv(socketNo, clientNo, 15, 0);
	printf("Connection accepted: Client: %s\n\n", clientNo);
	return 0;
}

//__GAME_INITIALISER__
//This function intialises the game by sending the piece selections to the server.

int gameInit(){
	char pSend[3];
	pSend[0] = clientPiece;
	pSend[1] = serverPiece;
//Send the pieces.
	send(socketNo, pSend, 3, 0);
}

//__GAME_PRINT__
//Prints the game board to the terminal.
int gamePrint(){
	for(int y = 0; y < 3; y++) {
			printf(" %c | %c | %c \n", board[y][0], board[y][1], board[y][2]);
			if(y != 2){
				printf("---|---|---\n");
			}			
	}

}


//__GAME_HANDLER__
//This is the function that handles the logic side of the game.
//It determines if a move is valid and moves there if so etc.

int gameHandler(int number){
//The for loop goes through each item in the array and is used to determine which
//cell the change.
	for(int y = 0; y < 3; y++) {
		for(int x = 0; x < 3; x++) {
			if(((y*3)+(x+1)) == number) {
				board[y][x] = clientPiece;
//The board is win checked after every move.
				if(winCheck(board)) {
					board[0][0] = 'W';
					winner = 1;
				}
				if (send(socketNo, board, 12, 0) < 0) {
					printf("ERROR: Could not write to socket\n");
					return -1;
				}
//Count is used to keep track of how many numbers have been chaged in the board.
				count++;
			}
		}
	}

//After the user has had a move, it waits for the server to reply.
	recv(socketNo, board, 12, 0);
	count++;
	gamePrint();

//Should a draw occur (count >= 9) then the winner window is created an shown.
	if(count >= 9 || board[0][0] == 'N') {
		printf("Stalemate!\n");
		winner = 3;
//If the game is not a draw then it is a win.
	} else {
		if(winCheck(board) && (winner != 1)) {
			winner = 2;
		}
		if(winner == 1) {
//Create the winner dialog and show with custom message.
			printf("You win!\n");
			clientScore++;
//The game is reset after each game.
		} else if (winner == 2) {
//Create the winner dialog and show with custom message.
			printf("You lose!\n");
			serverScore++;
		}
	}
}

//__END_GAME_HANDLER__



int main(int argc, char *argv[]){
    serverInit(argv[1]);

	//Initialise the game with the server.
	gameInit();

	gamePrint();

	while(winner == 0){
		int i;
		printf("Choose a position: ");
		scanf("%d", &i);
		gameHandler(i);
	}

	gameReset();







}