//==============================================================================
//TIC TAC TOE SERVER
//==============================================================================

//__INCLUDE_STATEMENTS__

#include <unistd.h>     //Standard headers used to import library functions to
#include <stdlib.h>     //be used in the program.
#include <stdio.h>
#include <string.h>
#include <time.h>

#include <sys/types.h>  //These headers are used for the server client libraries.
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

//__END_INCLUDE__


//__GLOBAL_VARIABLE_DECLARATIONS__
//The pieces (X or O) are defined globally so that they are always avaliable to
//any function that requires them. As they are only 1 char each, this is not an issue.

char clientPiece;
char serverPiece;

//__END_GLOBAL_VARIABLE_DECLARATIONS__




//__CHECK_BOARD__
//This function plays a vital role in the artificial intelligence of the server.
//Just like in the server it checks to see if there are any winning combinations on the board.
//The AI is inside the clientHandler function.

int checkBoard(char board[3][3]){
	if((board[0][0]==board[1][1] && board[0][0]==board[2][2]) ||
	   (board[0][2]==board[1][1] && board[0][2]==board[2][0])) { return 1; }
	for(int line = 0; line <=2; line++) {
		if((board[line][0]==board[line][1] && board[line][0]==board[line][2])||
		   (board[0][line]==board[1][line] && board[0][line]==board[2][line])) {
			return 1;
		}
	}
	return 0;
}

//__END_CHECK_BOARD




//__CLIENT_HANDLER__
//This function is a handler for each client. It connects to the client and then
//manages the game functions along with AI.

void *clientHandler(int socket_desc){
//Random is seeded for each client that connects.
	srand(time(NULL));
//Board is initialised.
	char board[3][3]  = {
		{'1','2','3'},
		{'4','5','6'},
		{'7','8','9'}
	};

//Variables are created and initialised.
	int socketNo = socket_desc;
	int win = 0;
	int size;
	int play = 1;
	int id = getpid();
	char pTemp[10];
	char process[20];

//Put the Process ID into a string to pass to the client as a 'client' number.
	sprintf(process, "%d", id);
//Send the client number to the client.
	send(socketNo, process, 15, 0);

//This is a loop to keep the games resetting if the user wants to play again.
	while(play) {
//Recieve the server and client pieces from when the client selectes them.
		recv(socketNo, pTemp, 3, 0);
		clientPiece = pTemp[0];
		serverPiece = pTemp[1];

		printf("%d received pieces, %c,%c\n", socketNo, clientPiece, serverPiece);

//This keeps the server playing whilst the game is going on.
		while(win != 1) {
			int ry;
			int rx;
//Guessed is a variable to keep track of whether the space on the board has been used yet.
			int guessed = 1;

//Waits to receive the clients move.
			if((size = recv(socketNo, board, 12, 0)) < 0) {
				printf("ERROR: Cannot read from socket\n");
				printf("Client %s disconnected!\n", process);
				fflush(stdout);
				return 0;
			} else if (size == 0) {
//If the size of the received data is 0, the client has disconnected.
				printf("Client %s disconnected!\n", process);
				fflush(stdout);
				win = 1;
				break;
			}
//Checks to see if the client has won the game from their end.
			if(board[0][0] == 'W') {
				win = 1;
				break;
			}


//__SERVER_AI__
//The server AI is used to determine where the computer should play its move.

			int winning = 0;
			int block = 0;

//Winning is prioritised for the server.
			for(int i = 0; i < 3; i++) {
				for(int j = 0; j < 3; j++) {
//Temp board is created to come up with temporary conditions to check for a win.
					char tempBoard[3][3];
					memcpy(tempBoard, board, sizeof(char)*9);
//If there is not already a piece in the board position the server places it's piece into temp.
					if(!(board[i][j] == clientPiece) && !(board[i][j] == serverPiece)) {
						tempBoard[i][j] = serverPiece;
//The board is then checked to see if this will win the game.
						if(checkBoard(tempBoard)) {
//If this is the case then the server moves here permanently and sends to the client.
							board[i][j] = serverPiece;
							printf("WINNING\n");
//stdout must be flushed in order to print to the screen.
							fflush(stdout);
							winning = 1;
//The servers move is sent to the client.
							if(send(socketNo, board, 12, 0) < 0) {
								printf("ERROR: Could not write socket\n");
								printf("Client %s disconnected", process);
								fflush(stdout);
							}
						}
					}
				}
			}

//If the server does not find a winning move, it will try and block the client.
			if(winning == 0) {
				for(int i = 0; i < 3; i++) {
					for(int j = 0; j < 3; j++) {
//Again the same process is used to check a temp board for a win.
						char tempBoard[3][3];
						memcpy(tempBoard, board, sizeof(char)*9);
//The difference is that this time the server uses the client piece to check for a win.
						if(!(board[i][j] == clientPiece) && !(board[i][j] == serverPiece)) {
							tempBoard[i][j] = clientPiece;
							if(checkBoard(tempBoard)) {
//If a block is found then the server places it's piece there.
								board[i][j] = serverPiece;
								printf("BLOCKING at %d %d\n", i, j);
								fflush(stdout);
								block = 1;
//Board is sent to the client.
								if(send(socketNo, board, 12, 0) < 0) {
									printf("ERROR: Could not write socket\n");
									printf("Client %s disconnected", process);
									fflush(stdout);
								}
							}
						}
					}
				}
			}

//If no winning or blocking move is found, the client will place randomly.

			if(winning == 0 && block == 0) {
//Count is used to determine if it is a stalemate.
				int count = 0;
				for(int y = 0; y < 3; y++) {
					for(int x = 0; x < 3; x++) {
						if((board[y][x] == clientPiece) || (board[y][x] == serverPiece)) {
							++count;
						}
					}
				}
				printf("count %d\n", count);
//Count willbe equal to 8 if stalemate as there will be one place remaining on the board for the final server piece.
				if(count >= 8) {
					board[0][0] = 'N';
//This informaiton is passed back to the client.
					if(send(socketNo, board, 12, 0) < 0) {
						printf("ERROR: Could not write socket\n");
						printf("Client %s disconnected", process);
						fflush(stdout);
						return 0;
					}
				}
//This while loop is used to keep generating random numbers and trying to place them.
//Once a space is free the server places it's piece there.
				while((guessed == 1) && (board[0][0] != 'N')) {
					ry = rand() % 3;
					rx = rand() % 3;
					if(!(board[ry][rx] == clientPiece) && !(board[ry][rx] == serverPiece)) {
						board[ry][rx] = serverPiece;
						guessed = 0;
						printf("RANDOM\n");
						fflush(stdout);
						if(send(socketNo, board, 12, 0) < 0) {
							printf("ERROR: Could not write socket\n");
							printf("Client %s disconnected", process);
							fflush(stdout);
							return 0;
						}
					}
				}
			}
		}
	}

	return 0;

}

//__END_CLIENT_HANDLER__




//__MAIN__
//The main function sets up up the server and starts lisntening for client connections.

int main(int argc, char **argv){
//Server initialisation.
	int sockFd, pid;
	int socketNo;

	struct sockaddr_in serv_addr;
	struct sockaddr_in cli_addr;

	int portNo = atoi(argv[1]);
	if(portNo < 1 || portNo > 65535)
	{
		fprintf(stderr, "ERROR: Port number invalid. Exiting...\n");
		exit(1);
	}


	printf("Initialising server\n");

//Open the socket.
	sockFd = socket(AF_INET, SOCK_STREAM, 0);

	if(sockFd < 0)
	{
		fprintf(stderr, "ERROR: Unable to open socket. Exiting...\n");
		exit(1);
	}

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portNo);

//Bind the socket.
	if(bind(sockFd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
		fprintf(stderr, "ERROR: Binding failure\n");
		exit(1);
	}

	printf("Server started on port: %d\n", portNo);


//CLIENT LISTENER
//Listens out for clients strying to connect.

	listen(sockFd, 10);
	socklen_t clilen = sizeof(cli_addr);

//While loops to keep accepting clients.
	while(1) {

//Accept client.
		socketNo = accept(sockFd, (struct sockaddr*)&cli_addr, &clilen);

//Error identification.
		if(socketNo < 0)
		{
			fprintf(stderr, "ERROR: Accept Failure\n");
			exit(2);
		}

//Fork to create a new process (this is to enable multi client).
		pid = fork();

//Error identification.
		if (pid < 0) {
			fprintf(stderr, "ERROR: Forking Error\n");
			exit(2);
		}

//If the connection is accepted and there are no errors, start the client handler.
		if (pid == 0) {
			close(sockFd);
			printf("Client %d connected!\n", getpid());
			fflush(stdout);
			clientHandler(socketNo);
			exit(0);
		} else {
//Else close the connection.
			close(socketNo);
		}
	}
}

//__END_MAIN__
