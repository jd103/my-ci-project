//------------------------------------------------------------------------------
//WIN CHECK FUNCTION
//------------------------------------------------------------------------------

#include "winCheck.h"

//__WIN_CHECK__
//This function checks the board and sees if there is a winner.

int winCheck(char board[3][3]) {
//This if statement checks the diagonals for a win.
	if((board[0][0]==board[1][1] && board[0][0]==board[2][2]) ||
	   (board[0][2]==board[1][1] && board[0][2]==board[2][0])) { return 1; }
//Then this for loop checks each row and column for a win.
	for(int line = 0; line <=2; line++) {
		if((board[line][0]==board[line][1] && board[line][0]==board[line][2])||
		   (board[0][line]==board[1][line] && board[0][line]==board[2][line])) {
			return 1;
		}
	}
//If there is no winner then 0 is returned.
	return 0;
}

//__END_WIN_CHECK__
