#include "remove.h"
#include <string.h>

void remove_line(char msg[])
{
	int len = strlen(msg);
	if(len > 0 && msg[len - 1] == '\n')
	{
		msg[len - 1] = '\0';
	}
}
