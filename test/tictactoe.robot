*** Settings ***
Documentation     Robot framework file for testing the Tic Tac Toe server
Library           Process
Library           ServerTestLib.py
Library           OperatingSystem
Test Setup        Start Server on Port 5000
Test Teardown     Stop All  

*** Variables ***
${SERVER_BIN}     ../build/server
${SERVER_PORT}    5000
${SERVER_IP}      127.0.0.1
${SERVER_PROC}    server
${SERVER_STDOUT}  srvOut
${SERVER_STDERR}  srvErr

${CLIENT_BIN}     ../build/cmdClient
${CLIENT_STDOUT}  cliOut
${CLIENT_STDERR}  cliErr
${CLIENT_PROC}    client

${TIMEOUT}        1
${RETRIES}        3

*** Test Cases ***
SRV-001
    [Documentation]  Test the server process runs.
    [Tags]  Server  
    Process Should Be Running  ${SERVER_PROC}

SRV-002
    [Documentation]  Test the server opens the port and listens for connections
    [Tags]  Server
    ${result}=  Check Host  ${SERVER_IP}  ${SERVER_PORT}  ${TIMEOUT}  ${RETRIES}
    Should Be True  ${result}

SRV-002a
    [Documentation]  Test the server opens the port and listens for connections [negative check]
    [Tags]  Server
    ${result}=  Check Host  ${SERVER_IP}  5001  ${TIMEOUT}  ${RETRIES}
    Should Be Equal  ${result}  ${FALSE}


SRV-003
    [Documentation]  Test a client is able to connect to the server and is assigned a number
    [Tags]  Server  Client
    Start Client and Connect to ${SERVER_IP}
    ${connection}=  Grep File  ${SERVER_STDOUT}  Client [0-9]* connected!
    Log  ${connection}

SRV-004
    [Documentation]  Test multiple clients are able to connect to the server simultaneously
    [Tags]  Server  Client
    Start Multiple Clients and Connect to ${SERVER_IP}
    #Start Client and Connect to ${SERVER_IP}
    ${connections}=  Grep File  ${SERVER_STDOUT}  Client [0-9]* connected!
    Log  ${connections}

    
    
*** Keywords ***
Start Server on Port ${port}
    Log To Console  Starting Server
    Start Process  ${SERVER_BIN}  ${port}  stderr=${SERVER_STDERR}  stdout=${SERVER_STDOUT}  alias=${SERVER_PROC}
    Sleep  1
    ${id}=  Get Process Id 
    [Return]    ${id}

Start Client and Connect to ${ip}
    Log To Console  Starting Client
    Start Process  ${CLIENT_BIN}  ${ip}  stderr=${CLIENT_STDERR}  stdout=${CLIENT_STDOUT}  alias=${CLIENT_PROC}
    Sleep  1
    ${id}=  Get Process Id 
    [Return]    ${id}

Start Multiple Clients and Connect to ${ip}
    Log to Console  Starting Multimple Clients
    Start Process  ${CLIENT_BIN}  ${ip}  stderr=${CLIENT_STDERR}  stdout=${CLIENT_STDOUT}  alias=${CLIENT_PROC} 1
    Start Process  ${CLIENT_BIN}  ${ip}  stderr=${CLIENT_STDERR}  stdout=${CLIENT_STDOUT}  alias=${CLIENT_PROC} 2
    ${id_1}=  Get Process Id  ${CLIENT_PROC} 1
    ${id_2}=  Get Process Id  ${CLIENT_PROC} 2
    @{ids}=  Create List  ${id_1}  ${id_2}

Stop All
    Log To Console  Terminating processes
    Terminate All Processes  kill=True






