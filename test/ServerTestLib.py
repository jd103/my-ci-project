import socket
import time

class ServerTestLib:
   def is_open(self, ip, port, timeout):
      s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      s.settimeout(timeout)
      try:
               s.connect((ip, int(port)))
               s.shutdown(socket.SHUT_RDWR)
               return True
      except:
               return False
      finally:
               s.close()

   def check_host(self, ip, port, timeout, retry):
      ipup = False
      for i in range(int(retry)):
               if self.is_open(str(ip), int(port), int(timeout)):
                     ipup = True
                     break
               else:
                     time.sleep(int(timeout))
      return ipup

